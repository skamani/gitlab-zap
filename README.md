# GitLab-Zap

This project demonstrates using OWASP ZAP as part of a GitLab pipeline.  

It uses command line options defined in the [Zaproxy documentation](https://www.zaproxy.org/docs/docker/about/).

Corrently configured to scan http://zero.webappsecurity.com/, emit the artifact in XML format and upload the artifact
as a job/pipeline artifact.  It could be set up to transfer artifact to downstream jobs for further processing.

### Benefits of using this approach
1. Shifting security left: Each of these scans can be run as early as committing new code or as needed based on specific tags
or branch definitions.

Developers have the ability to see the results of the scan earlier.

2. All artifacts remain in GitLab for as long as needed and discarded.  Also auditability is built in with knowledge of who 
ran the pipeline and when the artifact was generated.

### Disadvantages of using this approach
1. There is no easy process to run a baseline scan and then perform a diff scan.

2. Results are in an XML artifact.  Further processing would be needed to make it digestible.  Perhaps converting to HTML.

3. No metrics are easily available on a dashboard.

4. Downstream management of Vulnerabilities is absent and so yet another tool would be needed to injest the output and manage the work.

